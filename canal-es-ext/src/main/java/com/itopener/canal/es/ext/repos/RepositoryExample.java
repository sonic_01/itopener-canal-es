package com.itopener.canal.es.ext.repos;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.itopener.canal.es.ext.model.ModelExample;

public interface RepositoryExample extends ElasticsearchRepository<ModelExample, Long> {

}
