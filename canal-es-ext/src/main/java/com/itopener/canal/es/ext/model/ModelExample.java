package com.itopener.canal.es.ext.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;

@Document(indexName = "canal_es_demo", type = "canal_es_model", indexStoreType = "fs", shards = 5, replicas = 1, refreshInterval = "-1")
public class ModelExample implements Serializable {

	/** */
	private static final long serialVersionUID = 4980638149364801243L;

	@Id
	private long id;
	
	private String name;
	
	@Field(type = FieldType.Date)
	private Timestamp createTime;
	
	@Field(type = FieldType.Date)
	private Date date;
	
	@GeoPointField
	private String point;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
}
